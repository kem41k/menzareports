﻿using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;

namespace MenzaReports
{
    public partial class Form1 : Form
    {
		string[] files;							// List of files in current folder
        string dateStart = "", dateEnd = "";	// Date of first and last lines in a Reports0 file
        string date1 = "", date2 = "";			// Interval dates
        string[] lines;							// Content from Reports0 file

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            label1.Visible = false;
            dateTimePicker_End.Visible = false;
            dataGridView_report.AllowUserToAddRows = false;

            // Report files check
            files = DataProc.checkFiles();
            // There are no files with reports
            if (files == null)
            {
                listBox_status.Items.Add("[" + DateTime.Now + "] ОШИБКА: файл с отчетом не найден!");
                disableElements();
            }
            // There is one or more files with reports (only last is needed)
            else
            {
                listBox_status.Items.Add("[" + DateTime.Now + "] We found " + files.Length + " file(s).");
                lines = DataProc.getContent(files[files.Length - 1]);
                // ========================== File is empty ==========================
                if (lines == null || lines.Length == 0)
                {
                    listBox_status.Items.Add("[" + DateTime.Now + "] ОШИБКА: файл с отчетом пустой!");
                    disableElements();
                }
                // ======================== File is not empty ========================
                else
                {
                    // Looking for first and last date in file
                    string[] temp = DataProc.getDateInterval(lines);
                    // Dates are found
                    if (temp[0] != null && temp[1] != null)
                    {
                        dateStart = temp[0];
                        listBox_status.Items.Add("[" + DateTime.Now + "] Первая дата в файле: " + dateStart);
                        dateEnd = temp[1];
                        listBox_status.Items.Add("[" + DateTime.Now + "] Последняя дата в файле: " + dateEnd);
                        // Set found dates on WinForm
                        dateTimePicker_Start.MinDate = Convert.ToDateTime(dateStart);
                        dateTimePicker_Start.MaxDate = Convert.ToDateTime(dateEnd);
                        dateTimePicker_Start.Value = Convert.ToDateTime(dateEnd);
                        dateTimePicker_End.MinDate = Convert.ToDateTime(dateStart);
                        dateTimePicker_End.MaxDate = Convert.ToDateTime(dateEnd);
                        dateTimePicker_End.Value = Convert.ToDateTime(dateEnd);
                    }
                    // Problem with date occured
                    else
                    {
                        listBox_status.Items.Add("[" + DateTime.Now + "] ОШИБКА: первая и/или последняя даты не распознаны!");
                        disableElements();
                    }
                }
            }
        }

        // Disable elements to forbid continious work
        private void disableElements()
        {
            radioButton_oneDay.Enabled = false;
            radioButton_period.Enabled = false;
            button_Save.Enabled = false;
            button_Report.Enabled = false;
        }

        // "One day" radiobutton click handler
        private void radioButton_oneDay_Click(object sender, EventArgs e)
        {
            if (radioButton_oneDay.Checked)
            {
                label1.Visible = false;
                dateTimePicker_Start.Visible = true;
                dateTimePicker_End.Visible = false;
                dateTimePicker_Start.Value = Convert.ToDateTime(dateEnd);
            }
            else if (radioButton_period.Checked)
            {
                label1.Visible = true;
                dateTimePicker_Start.Visible = false;
                dateTimePicker_End.Visible = true;
                if (Convert.ToDateTime(dateEnd).AddDays(-30) >= Convert.ToDateTime(dateStart))
                    dateTimePicker_Start.Value = Convert.ToDateTime(dateEnd).AddDays(-30);
                else
                    dateTimePicker_Start.Value = Convert.ToDateTime(dateStart);
                dateTimePicker_End.Value = Convert.ToDateTime(dateEnd);
            }
        }

        // "Period" radiobutton click handler
        private void radioButton_period_Click(object sender, EventArgs e)
        {
            if (radioButton_oneDay.Checked)
            {
                label1.Visible = false;
                dateTimePicker_End.Visible = false;
                dateTimePicker_Start.Value = Convert.ToDateTime(dateEnd);
            }
            else if (radioButton_period.Checked)
            {
                label1.Visible = true;
                dateTimePicker_End.Visible = true;
                if (Convert.ToDateTime(dateEnd).AddDays(-30) >= Convert.ToDateTime(dateStart))
                    dateTimePicker_Start.Value = Convert.ToDateTime(dateEnd).AddDays(-30);
                else
                    dateTimePicker_Start.Value = Convert.ToDateTime(dateStart);
                dateTimePicker_End.Value = Convert.ToDateTime(dateEnd);
            }
        }

        // Report preparing and presenting
        private void button_Report_Click(object sender, EventArgs e)
        {
            dataGridView_report.Columns.Clear();
            button_Save.Enabled = true;
            
            // ======================== Start of data processing ========================

            date1 = dateTimePicker_Start.Value.AddDays(1).ToString().Split(' ')[0];
            date2 = dateTimePicker_End.Value.AddDays(1).ToString().Split(' ')[0];
            try
            {
                // ========================= One day report =============================
                if (radioButton_oneDay.Checked)
                {
                    listBox_status.Items.Add("[" + DateTime.Now + "] Подготовка отчета за " + Convert.ToDateTime(date1).AddDays(-1).ToString().Split(' ')[0]);
                    var i = DataProc.getContentOnDate(lines, date1);
                    
                    // No data on chosen day
                    if (i == -1)
                    {
                        dataGridView_report.Columns.Clear();
                        button_Save.Enabled = false;
                        listBox_status.Items.Add("[" + DateTime.Now + "] ОШИБКА: Данные за " + Convert.ToDateTime(date1).AddDays(-1).ToString().Split(' ')[0] + " не обнаружены. Выберте другой файл!");
                    }
                    // Data is found
                    else
                    {
                        dataGridView_report.DataSource = DataProc.getOneDayReport(lines, i);
                        dataGridView_report.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
                    }
                }
                // ======================== End of one day report ========================

                // ======================== Period report ========================
                else if (radioButton_period.Checked)
                {
                    // Check the interval (date1 - date2) for correctness
                    if (dateTimePicker_Start.Value >= dateTimePicker_End.Value)
                        listBox_status.Items.Add("[" + DateTime.Now + "] ОШИБКА: выберите вторую дату после первой!");
                    else
                    {
                        listBox_status.Items.Add("[" + DateTime.Now + "] Подготовка отчета за период с " + Convert.ToDateTime(date1).AddDays(-1).ToString().Split(' ')[0] + " по " + Convert.ToDateTime(date2).AddDays(-1).ToString().Split(' ')[0]);
                        dataGridView_report.DataSource = DataProc.getIntervalReport(lines, date1, date2);
                        dataGridView_report.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);
                    }
                }
                // ======================== End of period report ========================

            }
            catch (Exception ex)
            {
                //listBox_status.Items.Add("[" + DateTime.Now + "] ERROR occured while data processing.");
                listBox_status.Items.Add("[" + DateTime.Now + "] " + ex.Message);
                listBox_status.TopIndex = listBox_status.Items.Count - 1;
            }

            // ======================== End of data processing ========================
        }

        // Save results to *.csv file
        private void button_Save_Click(object sender, EventArgs e)
        {
            try
            {
                SaveFileDialog saveFileDialog = new SaveFileDialog();
                saveFileDialog.Filter = "CSV|*.csv";
                if (radioButton_oneDay.Checked)
                    saveFileDialog.FileName = Convert.ToDateTime(date1).AddDays(-1).ToString().Split(' ')[0] + " АГЭУ Менза отчет";
                else if (radioButton_period.Checked)
                    saveFileDialog.FileName = Convert.ToDateTime(date1).AddDays(-1).ToString().Split(' ')[0] + "-" + Convert.ToDateTime(date2).AddDays(-1).ToString().Split(' ')[0] + " АГЭУ Менза отчет";

                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    StreamWriter streamWriter = new StreamWriter(saveFileDialog.FileName, false, Encoding.GetEncoding(1251));
                    // Копируем все данные из dataGridView в файл
                    if (radioButton_oneDay.Checked)
                        streamWriter.WriteLine("Отчет за " + Convert.ToDateTime(date1).AddDays(-1).ToString().Split(' ')[0] + "\n");
                    else if (radioButton_period.Checked)
                    {
                        streamWriter.WriteLine("Отчет за " + Convert.ToDateTime(date1).AddDays(-1).ToString().Split(' ')[0] + "-" + Convert.ToDateTime(date2).AddDays(-1).ToString().Split(' ')[0] + "\n");
                        streamWriter.WriteLine("Дата;АГЭУ, кВт*ч;ДГУ1, кВт*ч;ДГУ2, кВт*ч;ФЭС, кВт*ч;Зар. АКБ, кВт*ч;Разр. АКБ, кВт*ч;СН, кВт*ч;ДГУ1 потр. из КХТ, кг;ДГУ1 потр. из бака, кг;ДГУ1 УРДТ, кг/кВт*ч;ДГУ2 потр. из КХТ, кг;ДГУ2 потр. из бака, кг;ДГУ2 УРДТ, кг/кВт*ч;PR, %");
                    }

                    for (int i = 0; i < dataGridView_report.Rows.Count; i++)
                    {
                        string line = "";
                        for (int j = 0; j < dataGridView_report.Columns.Count; j++)
                            line += dataGridView_report[j, i].Value + ";";
                        streamWriter.WriteLine(line);
                    }
                    streamWriter.Close();
                    listBox_status.Items.Add("[" + DateTime.Now + "] Сохранен файл: " + saveFileDialog.FileName);
                }
                else 
                    listBox_status.Items.Add("Файл с отчетом не был сохранен!");
            }
            catch
            {
                listBox_status.Items.Add("[" + DateTime.Now + "] При сохранении файла произошла ошибка.");
            }
        }
    }
}