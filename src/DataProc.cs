﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;

namespace MenzaReports
{
    public static class DataProc
    {
        // Get content from file 'filename'
        public static string[] getContent(string filename)
        {
            StreamReader reader = null;
            try
            {
                reader = new StreamReader(filename);
                string[] lines = reader.ReadToEnd().Split('\r');
                reader.Close();
                return lines;
            }
            catch
            {
                if (reader != null) reader.Close();
                return null;
            }
        }

        // Return first date in string array 'lines'
        public static string[] getDateInterval(string[] lines)
        {
            string[] dates = { null, null };
            int i = 0;          // Number of line in file
            bool sign = false;  // Becomes true if the date will be found

            // Looking for first date
            while (i < lines.Length && !sign)
            {
                string[] lineElems = lines[i].Split(';');
                for (var k = 0; k < lineElems.Length; k++)
                    lineElems[k] = deleteQuoteMarks(lineElems[k]);
                if (lineElems[0].Contains("DGU") || lineElems[0].Contains("PR") || lineElems[0].Contains("AKB") || lineElems[0].Contains("Sobstv") || lineElems[0].Contains("Vyrabotka"))
                {
                    dates[0] = lineElems[1].Split(' ')[0];
                    sign = true;
                }
                i++;
            }

            if (sign)
            {
                dates[0] = Convert.ToDateTime(dates[0]).AddDays(-1).ToString().Split(' ')[0];       // Convert to real date
                sign = false;
            }

            // Looking for last date
            i = lines.Length - 1;
            while (i > 0 && !sign)
            {
                string[] lineElems = lines[i].Split(';');
                for (var k = 0; k < lineElems.Length; k++)
                    lineElems[k] = deleteQuoteMarks(lineElems[k]);
                if (lineElems[0].Contains("DGU") || lineElems[0].Contains("PR") || lineElems[0].Contains("AKB") || lineElems[0].Contains("Sobstv") || lineElems[0].Contains("Vyrabotka"))
                {
                    dates[1] = lineElems[1].Split(' ')[0];
                    sign = true;
                }
                i--;
            }
            if (sign) dates[1] = Convert.ToDateTime(dates[1]).AddDays(-1).ToString().Split(' ')[0];     // Convert to real date

            return dates;
        }

        // Look for content on chosen date 'date' in string array 'lines'
        public static int getContentOnDate(string[] lines, string date)
        {
            int i = 0;
            while (i < lines.Length - 1)
            {
                string[] temp = lines[i].Split(';');
                for (var k = 0; k < temp.Length; k++)
                    temp[k] = deleteQuoteMarks(temp[k]);
                if (temp[1].Contains(date) && (temp[0].Contains("DGU") || temp[0].Contains("PR") || temp[0].Contains("AKB") || temp[0].Contains("Sobstv") || temp[0].Contains("Vyrabotka")))
                {
                    // Data on required date is found
                    i++;
                    return i;
                }
                i++;
            }
            // Return -1, if content on chosen date was not found
            return -1;
        }

        /* Returns required data on one day to fill in datagrid
         * Parameters:
         * 1) lines - string array;
         * 2) lineNumber - number of first line with required date 
         */
        public static DataTable getOneDayReport(string[] lines, int lineNimber)
        {
            string[] titlesForGrid = { "Выработка АГЭУ, кВт*ч", "Выработка ДГУ1, кВт*ч", "Выработка ДГУ2, кВт*ч", "Выработка ФЭС, кВт*ч", "Заряд АКБ, кВт*ч", "Разряд АКБ, кВт*ч", "Собств. нужды, кВт*ч", "ДГУ1 потребл. топлива, кг", "ДГУ1 УРДТ, кг/кВт*ч", "ДГУ1 моточасы, ч", "ДГУ2 потребл. топлива, кг", "ДГУ2 УРДТ, кг/кВт*ч", "ДГУ2 моточасы, ч", "PR, %" };
            string[] titlesFromFile = {"Vyrabotka_AGEY_kWh","Vyrabotka_DGU1_kWh","Vyrabotka_DGU2_kWh","Vyrabotka_FES_kWh","Zaryad_AKB_kWh","Razryad_AKB_kWh","Sobstv_nyzhdy_kWh","DGU1_toplivo_posle_baka_kg","DGU1_YRDT_kg_kWh","DGU_1_run_hours","DGU2_toplivo_posle_baka_kg","DGU2_YRDT_kg_kWh","DGU_2_run_hours","PR_%"};

            // Prepare a table
            DataTable table = new DataTable();
            table.Columns.Add("Параметр");
            table.Columns.Add("Значение");
            for (int i = 0; i < titlesForGrid.Length; i++)
                table.Rows.Add(titlesForGrid[i], 0);
            
            // Search for required data
            string[] temp = lines[lineNimber].Split(';');
            for (var k = 0; k < temp.Length; k++)
                temp[k] = deleteQuoteMarks(temp[k]);

            var counter = 0;    // Amount of parameters is 14, so it is a limiter
            while ((temp[0].Contains("DGU") || temp[0].Contains("PR") || temp[0].Contains("AKB") || temp[0].Contains("Sobstv") || temp[0].Contains("Vyrabotka")) && counter < 14)
            {
                // Look for certain title in file
                for (int j = 0; j < titlesFromFile.Length; j++)
                    if (temp[0].Contains(titlesFromFile[j]))        // Certain title is found
                    {
                        table.Rows[j][1] = Math.Round(Convert.ToDouble(temp[2]), 2);
                        counter++;
                        break;
                    }
                // Go to next line
                lineNimber++;
                temp = lines[lineNimber].Split(';');
                for (var k = 0; k < temp.Length; k++)
                    temp[k] = deleteQuoteMarks(temp[k]);
            }

            return table;
        }

        // Returns required data on period (date1-date2) to fill in datagrid from 'lines' array
        public static DataTable getIntervalReport(string[] lines, string date1, string date2)
        {
            string[] titlesForGrid = { "Дата", "АГЭУ, кВт*ч", "ДГУ1, кВт*ч", "ДГУ2, кВт*ч", "ФЭС, кВт*ч", "Зар. АКБ, кВт*ч", "Разр. АКБ, кВт*ч", "СН, кВт*ч", "ДГУ1 потр. из КХТ, кг", "ДГУ1 потр. из бака, кг", "ДГУ1 УРДТ, кг/кВт*ч", "ДГУ1 моточасы, ч", "ДГУ2 потр. из КХТ, кг", "ДГУ2 потр. из бака, кг", "ДГУ2 УРДТ, кг/кВт*ч", "ДГУ2 моточасы, ч", "PR, %" };
            string[] titlesFromFile = { "Vyrabotka_AGEY_kWh", "Vyrabotka_DGU1_kWh", "Vyrabotka_DGU2_kWh", "Vyrabotka_FES_kWh", "Zaryad_AKB_kWh", "Razryad_AKB_kWh", "Sobstv_nyzhdy_kWh", "DGU1_toplivo_do_baka_kg", "DGU1_toplivo_posle_baka_kg", "DGU1_YRDT_kg_kWh", "DGU_1_run_hours", "DGU2_toplivo_do_baka_kg", "DGU2_toplivo_posle_baka_kg", "DGU2_YRDT_kg_kWh", "DGU_2_run_hours", "PR_%" };

            // Prepare a table
            DataTable table = new DataTable();
            for (int i = 0; i < titlesForGrid.Length; i++)
                table.Columns.Add(titlesForGrid[i]);

            // Calculating number of rows in table (by calculating an amount of days in interval)
            var report_lines = 0;
            string currDate = date1;
            while (Convert.ToDateTime(currDate).AddDays(-1) <= Convert.ToDateTime(date2).AddDays(-1))
            {
                table.Rows.Add(new Object[] { Convert.ToDateTime(currDate).AddDays(-1).ToString().Split(' ')[0], 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 });
                report_lines++;
                currDate = Convert.ToDateTime(currDate).AddDays(1).ToString().Split(' ')[0];
            }

            // Get the data from interval
            var lineNumber = 1;     // Number of current line in 'lines' array
            currDate = date1;
            while (lineNumber < lines.Length - 1 && Convert.ToDateTime(currDate) <= Convert.ToDateTime(date2))
            {
                string[] temp = lines[lineNumber].Split(';');
                for (var k = 0; k < temp.Length; k++)
                    temp[k] = deleteQuoteMarks(temp[k]);
                
                // Extract the date from current line
                currDate = temp[1].Split(' ')[0];
                if (Convert.ToDateTime(currDate) > Convert.ToDateTime(date2)) break;
                
                // Extract parameters from lines
                bool sign = true;
                if (Convert.ToDateTime(currDate) >= Convert.ToDateTime(date1))
                {
                    if (temp[0].Contains("DGU") || temp[0].Contains("PR") || temp[0].Contains("AKB") || temp[0].Contains("Sobstv") || temp[0].Contains("Vyrabotka"))
                    {
                        // Get the row number to fill new value
                        TimeSpan ts = Convert.ToDateTime(currDate) - Convert.ToDateTime(date1);
                        var rowNumber = ts.Days;
                        
                        // Look for certain title in file
                        for (int j = 0; j < titlesFromFile.Length; j++)
                            if (temp[0].Contains(titlesFromFile[j]))        // Certain title is found
                            {
                                table.Rows[rowNumber][j + 1] = Math.Round(Convert.ToDouble(temp[2]), 2);
                                break;
                            }
                    }
                }
                lineNumber++;
            }

            // Add last row with total or average numbers
            if (report_lines > 0)
            {
                table.Rows.Add(new Object[] {"ИТОГО", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0});
                for (var j = 1; j < table.Columns.Count; j++)
                {
                    double temp = 0;
                    for (lineNumber = 0; lineNumber < report_lines; lineNumber++)
                        temp += Convert.ToDouble(table.Rows[lineNumber][j]);
                    // Calculate the average value for 'URDT' and 'PR' parameters
                    if (j == 10 || j == 14 || j == table.Columns.Count - 1)
                        temp /= report_lines;
                    // Put last values for machine hours parameters
                    if (j == 11 || j == 15)
                        temp = Convert.ToInt32(table.Rows[report_lines - 1][j]);
                    table.Rows[report_lines][j] = Math.Round(temp, 1);
                }
            }

            return table;
        }

        // Looking for files with reports
        public static string[] checkFiles()
        {
            string[] files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "*.csv", SearchOption.TopDirectoryOnly);
            if (files.Length == 0) return null;
            else return files;
        }

        // Delete quote marks from string 'line'
        private static string deleteQuoteMarks(string line)
        {
            if (line != null && line.Length > 0)
            {
                if (line[0] == '"')
                    line = line.Substring(1);
                if (line[line.Length - 1] == '"')
                    line = line.Substring(0, line.Length - 1);
            }
            return line;
        }
    }
}
