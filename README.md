# Menza Reports #

### General description ###

Simple program for data processing obtained from Menza power plant monitoring system. 
Reports0 file is updated every 8 hours. The program processed the file and presents information about energy production in a convenient form.

### Input and expected output ###
* Input : Reports0 file (should be in the same folder).
* Output : processed data that could be saved into *.csv file.